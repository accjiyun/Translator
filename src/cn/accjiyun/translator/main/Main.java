package cn.accjiyun.translator.main;

import java.awt.EventQueue;

import cn.accjiyun.translator.jframe.Window;
import cn.accjiyun.translator.model.TextRoom;

public class Main {
	
	public static TextRoom textRoom = new TextRoom();
	public static Window frame = new Window();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
 	}
	
}
