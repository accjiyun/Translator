package cn.accjiyun.translator.jframe;

import java.util.Map;

import cn.accjiyun.translator.model.TextRoom;

public class CommandlineDisplay {

	public CommandlineDisplay(TextRoom textRoom) {
		
		System.out.println("originaString:------->" + textRoom.getOriginaString());
		
		System.out.println("Error:" + textRoom.getErrorCode());

		System.out.println("\nus-phonetic:[" + textRoom.getUsphonetic() + "]");
		System.out.println("uk-phonetic:[" + textRoom.getUkphonetic() + "]");

		System.out.println("\ntranslation:\n" + textRoom.getTranslation());

		System.out.println("\nbasicExplains:\n" + textRoom.getBasicExplains());

		System.out.println("\nwebExplains:\n");

		for (Map.Entry<String, StringBuilder> entry : textRoom.getWebExplains().entrySet()) {
			System.out.println(entry.getKey() + ":\n" + entry.getValue());
		}
	}

}
