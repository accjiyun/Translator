package cn.accjiyun.translator.jframe;

import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import cn.accjiyun.translator.control.Control;
import cn.accjiyun.translator.model.WordsList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField searchTextField;
	private JTextPane resultTextPane;
	private JScrollPane resultScrollPane;
	
	private static int mouseOnSceenX, mouseOnSceenY;
	private static int jframeX, jframeY;
	
	WordsList wordList = new WordsList();
	private JButton nextWordButton;
	private JButton lastWordButton;
	
	public JTextPane getTextPane() {
		return resultTextPane;
	}
	
	/**
	 * Create the frame.
	 */
	public Window() {
		setTitle("英语词典 V1.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 922, 599);
//		setBackground(new Color(255, 255, 255, 120));
		contentPane = new JPanel();
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (getWidth() != (int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()) {
					setLocation(jframeX + (e.getXOnScreen() - mouseOnSceenX), jframeY + (e.getYOnScreen() - mouseOnSceenY));
				}
			}
		});
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mouseOnSceenX = e.getXOnScreen();
				mouseOnSceenY = e.getYOnScreen();
				jframeX = getX();
				jframeY = getY();
			}
		});
		contentPane.setOpaque(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel searchPanel = new JPanel();
		searchPanel.setOpaque(false);
		searchPanel.setLayout(new BorderLayout(0, 0));
		
		searchTextField = new JTextField();
		searchTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (isSearchEnpty()) {
					return;
				}
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER && arg0.isControlDown()) {
					try {
						String urlTest = "http://dict.youdao.com/search?q="+ searchTextField.getText() +"&keyfrom=fanyi.smartResult";
						java.net.URI uri = 
								new java.net.URI(urlTest);
						java.awt.Desktop.getDesktop().browse(uri);
						resultTextPane.setText("");
					} catch (URISyntaxException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else if (arg0.getKeyCode() == KeyEvent.VK_ENTER && !arg0.isControlDown()){
					resultTextPane.setText("");
					wordList.addWord(searchTextField.getText());
					new Control(searchTextField.getText());
				}
				setButtonStatus();
			}
		});
		searchTextField.setFont(new Font("宋体", Font.PLAIN, 32));
		searchPanel.add(searchTextField);
		searchTextField.setColumns(10);
		
		JPanel resultPanel = new JPanel();
		resultPanel.setOpaque(false);
		resultPanel.setLayout(new BorderLayout(0, 0));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(resultPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 866, Short.MAX_VALUE)
						.addComponent(searchPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 866, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(searchPanel, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(resultPanel, GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		resultScrollPane = new JScrollPane();
		resultScrollPane.setOpaque(false);
		resultPanel.add(resultScrollPane, BorderLayout.CENTER);
		
		resultTextPane = new JTextPane();
		resultScrollPane.setViewportView(resultTextPane);
		resultTextPane.setFont(new Font("宋体", Font.PLAIN, 20));
		
		JLabel label = new JLabel("");
		label.setToolTipText("词典查询（快捷键：Enter）");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (isSearchEnpty()) {
					return;
				}
				resultTextPane.setText("");
				wordList.addWord(searchTextField.getText());
				new Control(searchTextField.getText());
				setButtonStatus();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
		});
		label.setIcon(new ImageIcon(Window.class.getResource("/cn/accjiyun/translator/resource/images/search.png")));
		searchPanel.add(label, BorderLayout.EAST);
		
		JPanel panel = new JPanel();
		searchPanel.add(panel, BorderLayout.WEST);
		panel.setLayout(new BorderLayout(0, 0));
		
		lastWordButton = new JButton("<");
		lastWordButton.setEnabled(false);
		lastWordButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String words = wordList.decrementCurrentWord();
				if (words != null) {
					searchTextField.setText(words);
					new Control(searchTextField.getText());
				}
				setButtonStatus();
			}
		});
		panel.add(lastWordButton, BorderLayout.WEST);
		
		nextWordButton = new JButton(">");
		nextWordButton.setEnabled(false);
		nextWordButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String words = wordList.IncrementCurrentWord();
				if (words != null) {
					searchTextField.setText(words);
					new Control(searchTextField.getText());
				}
				setButtonStatus();
			}
		});
		setButtonStatus();
		panel.add(nextWordButton, BorderLayout.EAST);
		contentPane.setLayout(gl_contentPane);
	}
	
	public boolean isSearchEnpty() {
		if (searchTextField.getText().isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setButtonStatus() {
		
		int sumWords = wordList.getSumWord();
		int currentNum = wordList.getCurrentWordsOrder();
		if (sumWords > 1 && currentNum != 1) {
			lastWordButton.setEnabled(true);
		} else {
			lastWordButton.setEnabled(false);
		}
		if (currentNum < sumWords) {
			nextWordButton.setEnabled(true);
		} else {
			nextWordButton.setEnabled(false);
		}
		
	}
}
