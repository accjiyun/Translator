package cn.accjiyun.translator.model;

public class WordsList {

	private String wordsList[] = new String[50];
	private int sumWords;
	private int currentWordsOrder;

	public void addWord(String words) {
		if (words.equals(wordsList[sumWords]) ) {
			return;
		}
		wordsList[++sumWords] = words;
		currentWordsOrder = sumWords;
	}

	public WordsList() {
		sumWords = currentWordsOrder = 0;
	}

	public String[] getWordList() {
		return wordsList;
	}

	public int getSumWord() {
		return sumWords;
	}
	
	public int getCurrentWordsOrder() {
		return currentWordsOrder;
	}

	public String decrementCurrentWord() {
		if (currentWordsOrder > 0) {
			return wordsList[--currentWordsOrder];
		} else {
			return null;
		}
	}

	public String IncrementCurrentWord() {
		if (currentWordsOrder < sumWords) {
			return wordsList[++currentWordsOrder];
		} else {
			return null;
		}
	}

}
