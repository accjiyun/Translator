package cn.accjiyun.translator.model;

import java.util.HashMap;
import java.util.Map;

public class TextRoom {

	private boolean isWord;
	private String originaString;
	private String usphonetic;
	private String ukphonetic;
	private StringBuilder basicExplains = new StringBuilder();
	private Map<String, StringBuilder> webExplains = new HashMap<String, StringBuilder>();
	private String translation;
	private String errorCode = "0";
	
	public TextRoom() {
		clear();
	}
	
	public void clear() {
		originaString = "";
		usphonetic = "";
		ukphonetic = "";
		basicExplains = new StringBuilder();
		webExplains = new HashMap<String, StringBuilder>();
		translation = "";
		errorCode = "0";
	}

	public String backErrorMessages() {
		if (errorCode.equals("0")) {
			return "正常";
		} else if (errorCode.equals("20")) {
			return "要翻译的文本过长";
		} else if (errorCode.equals("30")) {
			return "无法进行有效的翻译";
		} else if (errorCode.equals("40")) {
			return "不支持的语言类型";
		} else if (errorCode.equals("50")) {
			return "无效的key";
		} else {
			return "无词典结果，仅在获取词典结果生效";
		}
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isWord() {
		return isWord;
	}

	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

	public void appendBasicExplains(String str) {
		basicExplains.append(str);
	}

	public void putWebExplains(String key, StringBuilder value) {
		webExplains.put(key, value);
	}

	public StringBuilder getBasicExplains() {
		return basicExplains;
	}

	public Map<String, StringBuilder> getWebExplains() {
		return webExplains;
	}

	public String getOriginaString() {
		return originaString;
	}

	public void setOriginaString(String originaString) {
		if (originaString.contains(" ")) {
			isWord = false;
		} else {
			isWord = true;
		}
		this.originaString = originaString;
	}

	public String getUsphonetic() {
		return usphonetic;
	}

	public void setUsphonetic(String usphonetic) {
		this.usphonetic = usphonetic;
	}

	public String getUkphonetic() {
		return ukphonetic;
	}

	public void setUkphonetic(String ukphonetic) {
		this.ukphonetic = ukphonetic;
	}

}
