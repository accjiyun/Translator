package cn.accjiyun.translator.util;

import java.io.File;

public class BufferFolder {

	 // 程序临时目录
	protected static String bufferFolder = System.getProperty("java.io.tmpdir") + "TranslatorTemp\\";

	public BufferFolder() {
		File bufferFile = new File(bufferFolder);
		if (!bufferFile.exists()) {
			bufferFile.mkdirs();
		}
	}
	
	public static String getBufferFolder() {
		return bufferFolder;
	}

	@Override
	public String toString() {
		return bufferFolder;
	}

}
